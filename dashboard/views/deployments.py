import os
import time
import yaml

from cloudify_rest_client.executions import Execution
from datetime import datetime
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, get_object_or_404
from django.views.decorators.http import require_GET, require_POST
from django.http import HttpResponse

from .. import forms, models, synchronize, tasks
from ..config import DashboardConfig
from ..utils import save_uploaded_file

def details(cloudify_client, deployment_id):
    """
    :param cloudify_client: existing CloudifyClient object
    :param deployment_id: ID of the deployment of which details we are interested in
    :return: details dictionary containing lists of executions, nodes and node instances for the deployment
    """
    details = {}
    executions = cloudify_client.executions.list(deployment_id)
    for execution in executions:
        execution['created_at'] = datetime.strptime(execution.created_at, "%Y-%m-%d %H:%M:%S.%f") # convert to datetime
    details['executions'] = executions #list(reversed(executions))
    details['nodes'] = cloudify_client.nodes.list(deployment_id)
    details['node_instances'] = cloudify_client.node_instances.list(deployment_id)
    return details


def outputs(cloudify_client, deployment_id):
    """
    :param cloudify_client: existing CloudifyClient object
    :param deployment_id: deployment_id: ID of the deployment of which outputs we want to know
    :return: a dictionary of all deployment outputs
    """
    deployment = models.Deployment.objects.get(name=deployment_id)
    if deployment.is_deployed():
        deployment_outputs = cloudify_client.deployments.outputs.get(deployment_id).outputs
        for key in deployment_outputs:
            return deployment_outputs[key]
    return None

# List all deployments belonging to chosen blueprint
@login_required
@require_GET
def list(request, component_id, wait=False, undeploy_in_progress=False):
    component = models.Component.objects.get(id=component_id)
    blueprint = component.blueprint
    cloudify_client = component.manager.get_client()

    if wait:
        "Waiting before fetching deployment list"
        time.sleep(3) # Before fetching deployment list, wait for a while

    deployments = cloudify_client.deployments.list(blueprint_id=blueprint.name)
    if not len(deployments):
        deployment = blueprint.deployment_set.first()
        deployment.undeploying = False
        deployment.save()

    for deployment in deployments:
        deployment['details'] = details(cloudify_client, deployment.id)
        deployment['outputs'] = outputs(cloudify_client, deployment.id)

    params = {'deployments': deployments,
              'blueprint': blueprint,
              'params_form': forms.ParameterForm(),
              'current_deployment': blueprint.deployment_set.first()}

    component.refresh_status()
    return render(request, 'dashboard/deployment/deployments.html', params)


def get(request, deployment_id):
    deployment = get_object_or_404(models.Deployment, deployment_id=deployment_id)
    synchronize.handle_deployments(deployment.blueprint)

    params = {'deployment': deployment,
              'blueprint': deployment.blueprint,
              'params_form': forms.ParameterForm()}

    return render(request, 'dashboard/deployment/deployments.html', params)

@login_required
@require_GET
def delete(request, blueprint_id, deployment_id):
    deployment = models.Deployment.objects.get(name=deployment_id)
    #try:
    deployment.undeploying = True
    deployment.save()
        #deployment.delete_from_manager()
    #except Exception as e:
    #    messages.error(request, str(e))

    tasks.undeploy.delay(deployment)
    messages.success(request, "Cleanup and undeploy procedure was started.")
    return redirect('list-deployments', deployment.blueprint.component.id)


def get_inputs_file_path(blueprint):
    component_name = blueprint.component.name
    folder = 'uploads/' + component_name + "/" + blueprint.name
    for file in os.listdir(folder):
        if file.endswith(".yaml") and "inputs" in file:
            return folder + "/" + file


# TODO make this better
@login_required
@require_GET
def start(request, blueprint_id):
    blueprint = models.Blueprint.objects.get(id=blueprint_id)
    deployment = models.Deployment.objects.get(blueprint=blueprint)
    #try:
    tasks.deploy.delay(deployment)
    messages.success(request, "Deployment was started on Cloudify manager. You can check current status of deployment in the 'Deployment info/Executions' tab.")
    #except Exception as e:
    #    messages.error(request, str(e))

    #return redirect('list-deployments-delayed', blueprint.component.id, True)
    return redirect('connect-manager', blueprint.component.manager.id)


@login_required
@require_POST
def create(request, blueprint_name):
    blueprint = models.Blueprint.objects.get(name=blueprint_name)
    deployment_form = forms.DeploymentForm(request.POST, request.FILES)

    if deployment_form.is_valid():
        deployment = deployment_form.save()  # Save to database
        deployment.local_inputs_filename += ".yaml"
        deployment.uploaded = False
        deployment.blueprint = blueprint
        deployment.save()
        inputs_file = request.FILES['file']

        save_uploaded_file(inputs_file, blueprint.application.name, blueprint_name, deployment.local_inputs_filename)
        messages.success(request, "Successfully created new deployment")
    else:
        messages.error(request, deployment_form.errors)

    return redirect('get-deployment', deployment.deployment_id)
