from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.views.decorators.http import require_http_methods, require_GET
from ..forms import ExtendedUserCreationForm


# Logs in authenticated user
@require_http_methods(["GET", "POST"])
def login_user(request):
    user = authenticate(username=request.POST['username'],
                        password=request.POST['password'])
    if user is not None:
        if user.is_active:  # Account was not cancelled
            login(request, user)
    user = User.objects.create_user
    user.save()


# Log out current user
@require_GET
def logout_user(request):
    logout(request)
    return HttpResponseRedirect('/login')


# Registers a new user
@require_http_methods(["GET", "POST"])
def register_user(request):
    # User provides his account settings
    if request.method == 'POST':
        form = ExtendedUserCreationForm(request.POST)
        if form.is_valid():
            new_user = form.save()
            user = authenticate(username=request.POST['username'],
                                password=request.POST['password1'])
            login(request, user)
            return HttpResponseRedirect('/index')
        else:
            messages.error(request, form.errors)
        return render(request, "registration/register.html", {'form': form})
    # GET, show registration form
    else:
        form = ExtendedUserCreationForm()
        return render(request, "registration/register.html", {'form': form})
