from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.views.decorators.http import require_GET

from .. import models

def get_events(request, deployment_name, execution_id):
    deployment = models.Deployment.objects.get(name=deployment_name)
    manager = deployment.blueprint.component.manager
    cloudify_client = manager.get_client()
    events = cloudify_client.events.get(execution_id, include_logs=True)[0]

    return render(request, 'dashboard/executionEvents.html', {'events': events})

    # @login_required
    # @require_GET
    # def cancel_execution (request, execution_id):
    #     try:
    #         DashboardConfig.restClient.cancel_execution(execution_id)
    #         messages.success(request, "Execution " + execution_id + " was cancelled.")
    #     except Exception, exc:
    #         messages.error(request, "An error ocurred when cancelling execution: " + str(exc))
    #
    #     return redirect('get-deployment', e.deployment.deployment_id)

    # @login_required
    # @require_POST
    # def create_execution(request, deployment_id):
    #     deployment = models.Deployment.objects.get(deployment_id=deployment_id)
    #     blueprint = deployment.blueprint
    #
    #     # User finished editing form (indicated by the presence of hidden field named finished)
    #     if request.POST.get('create_execution') :
    #         params = None
    #         if request.POST.get('finished'):
    #             num_extra_params = request.POST.get('extra_params_count')
    #
    #             operation = request.POST.get('value')
    #             params = dict([("operation", operation)])
    #
    #             # Gather extra parameters
    #             for index in range (int(num_extra_params)):
    #                 param_key = request.POST.get('extra_key_{index}'.format(index=index))
    #                 param_value = request.POST.get('extra_value_{index}'.format(index=index))
    #                 params[param_key] = param_value
    #
    #         run_execution(request, deployment_id, params=params)
    #         return redirect ('get-deployment', deployment_id)
    #     else:
    #         params_form = forms.ParameterForm(request.POST, extra=request.POST.get('extra_params_count'))
    #         execution_form = forms.ExecutionForm(default=models.Execution.EXECUTE)
    #
    #         params = {'deployment': deployment,
    #                  'params_form': params_form,
    #                  'execution_form': execution_form,
    #                  'form': forms.DeploymentForm(),
    #                  'blueprint': blueprint,
    #                  'blueprint_deployments': blueprint.deployment_set.all() }
    #
    #         return render(request, 'dashboard/deployments.html', params)

    # def run_execution (request, deployment_id, **kwargs):
    #     params = kwargs.pop('params', None)
    #     execution_form = forms.ExecutionForm(request.POST)
    #
    #     if execution_form.is_valid():
    #         e = execution_form.save()
    #         e.deployment = models.Deployment.objects.get(deployment_id=deployment_id)
    #
    #         try:
    #             cfy_e = DashboardConfig.restClient.start_execution(e, params=params)
    #             synchronize.create_execution(cfy_e, e.deployment)
    #             messages.success(request, "New execution was started.")
    #         except Exception, exc:
    #              e.delete()
    #              messages.error(request, str(exc))
    #     else:
    #         messages.error(request, execution_form.errors)
