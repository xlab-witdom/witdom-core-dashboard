import logging
import os
import shutil

import yaml
from cloudify_rest_client.exceptions import CloudifyClientError
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.views.decorators.http import require_GET, require_POST

from .. import forms, models, utils

@login_required
@require_GET
def show_files(request, blueprint_name):
    blueprint = get_object_or_404(models.Blueprint, name=blueprint_name)
    deployment = get_object_or_404(models.Deployment, blueprint=blueprint)
    scripts = blueprint.script_set.all()
    return render(request, 'dashboard/blueprint/blueprintFiles.html', {"deployment" : deployment,
                                                              "blueprint" : blueprint,
                                                              "scripts" : scripts})


@login_required
@require_GET
def get_candidates(request, blueprint_name):
    blueprint = get_object_or_404(models.Blueprint, name=blueprint_name)
    blueprint_candidates = utils.get_candidates(blueprint, "blueprint")

    return render(request, 'dashboard/blueprint/blueprintCandidates.html',
                  {"blueprint": blueprint, "blueprintCandidates": blueprint_candidates})


def assign_blueprint_file(request, blueprint_name, filename):
    blueprint = get_object_or_404(models.Blueprint, name=blueprint_name)

    blueprint.local_filename = filename
    blueprint.local_filepath = utils.get_candidate_path(blueprint_name, filename)
    blueprint.save()

    # Automatically assign scripts referenced in this blueprint
    scripts, paths = utils.find_script_names_in_blueprint(blueprint)
    for i, script in enumerate(scripts):
        s = models.Script(name=script, filepath=paths[i], blueprint=blueprint)
        s.save()

    messages.success(request, 'YAML file ' + filename + ' successfully assigned to blueprint ' + blueprint_name + '.')

    return redirect('get-blueprint', blueprint_name, 'blueprint')


#         s = models.Script(name=script_name, blueprint=b)
#         s.save()
#         utils.save_uploaded_file(script_file, b.application.name, b.name, script_name, script=True)

# deployment = deployment_form.save()  # Save to database
# deployment.local_inputs_filename += ".yaml"
# deployment.uploaded = False
# deployment.blueprint = blueprint
# deployment.save()
#
# save_uploaded_file(inputs_file, blueprint.application.name, blueprint_name, deployment.local_inputs_filename)

@login_required
@require_POST
# TODO rename to deploy component, refactor
def create(request, component_id):
    form = forms.BlueprintForm(request.POST, request.FILES)
    print str(request.FILES.keys())
    if not 'blueprint_file' in request.FILES.keys() or not 'blueprint_inputs_file' in request.FILES.keys():
        messages.error(request, 'Please provide YAML files (blueprint and inputs).')
        return redirect('show-component', component_id)

    blueprint_name = request.POST['name']
    blueprint_file = request.FILES['blueprint_file']
    deployment_name = request.POST['deployment_name']
    blueprint_inputs_file = request.FILES['blueprint_inputs_file']

    if form.is_valid():  # Validates file extension
        if models.Blueprint.objects.filter(name=blueprint_name).exists():  # Blueprint with this name already exists
            messages.error(request, 'Blueprint with identical name already exists. Please choose another name.')
        elif models.Deployment.objects.filter(
                name=deployment_name).exists():  # Deployment with this name already exists
            messages.error(request, 'Deployment with identical name already exists. Please choose another name.')
        else:
            try:
                component = models.Component.objects.get(id=component_id)
                # Save all files to server
                print "CLEANED DATA: " + str(form.cleaned_data.keys())
                if request.FILES.has_key('script_file'):
                    script_file = request.FILES.get('script_file')
                    print script_file
                    utils.save_uploaded_file(script_file, component.name, blueprint_name, script_file.name, script=True)
                blueprint_path = utils.save_uploaded_file(blueprint_file, component.name, blueprint_name,
                                                          blueprint_file.name)
                print "saved blueprint"
                utils.save_uploaded_file(blueprint_inputs_file, component.name, blueprint_name,
                                         blueprint_inputs_file.name)
                print "saved blueprint inputs"

                # Try to upload this blueprint to Cloudify manager
                # Try to upload this deployment immediately to Cloudify manager
                # Inputs file must be converted to dictionary
                cloudify_client = component.manager.get_client()
                cloudify_client.blueprints.upload(blueprint_path, blueprint_name)

                # If everything went OK with cloudify, save models
                new_blueprint = models.Blueprint(name=blueprint_name, local_filename=blueprint_file.name)
                new_blueprint.component = component
                new_blueprint.save()
                new_deployment = models.Deployment(name=deployment_name, blueprint=new_blueprint,
                                                   local_inputs_filename=blueprint_inputs_file.name)
                new_deployment.save()
                print "Deployment saved"
                new_deployment.upload_to_manager()
                print "Deployment uploaded to manager"
                new_deployment.run()
                print "Deployment installed"

                if request.FILES.has_key('script_file'):
                    script = models.Script(name=script_file.name, blueprint=new_blueprint)
                    script.save()
            except CloudifyClientError as err:
                messages.error(request, err.message)
    else:
        messages.error(request, form.errors)

    return redirect('show-component', component_id)


# Compresses directory holding the component blueprints and serves it to end user
@login_required
@require_GET
def serve(request, blueprint_name):
    blueprint = models.Blueprint.objects.get(name=blueprint_name)

    import zipfile
    # Search in downloads directory
    if blueprint.downloaded:
        if not os.path.isdir('downloads'):
            os.makedirs('downloads')
        parent_path = 'downloads/'
        zipfile_path = blueprint_name + '.zip'
    # Search in uploads directory
    else:
        parent_path = 'uploads/' + blueprint.component.name
        zipfile_path = blueprint_name + '/' + blueprint_name + '.zip'

    curr_dir = os.getcwd()
    os.chdir(parent_path)

    # Create zip archive containing a folder holding blueprint files
    zf = zipfile.ZipFile(zipfile_path, 'w')
    utils.zipdir(blueprint_name, zf)
    zf.close()
    zipfile = open(zipfile_path, 'r')

    response = HttpResponse(zipfile, content_type='application/zip')
    response['Content-Disposition'] = 'attachment; filename="%s.zip' % blueprint_name

    zipfile.close()
    os.remove(zipfile_path)  # Cleanup
    os.chdir(curr_dir)

    return response  # Serve zip archive to end user

@login_required
@require_GET
def link(request, component_id, blueprint_id):
    component = models.Component.objects.get(id=component_id)
    blueprint = models.Blueprint.objects.get(id=blueprint_id)
    blueprint.component = component
    blueprint.download_remote_files()
    blueprint.associate_files()
    blueprint.save()

    cloudify_client = component.manager.get_client()
    deployments = cloudify_client.deployments.list(blueprint_id=blueprint.name)
    if len(deployments) is 0:
        new_deployment_name = "{}-deployment".format(blueprint.name)
        blueprint_deployment = models.Deployment(name=new_deployment_name, blueprint=blueprint)
        blueprint_deployment.associate_inputs_file()
        blueprint_deployment.save()
    else:
        for deployment in deployments:
            if not models.Deployment.objects.filter(name=deployment.id).exists():
                blueprint_deployment = models.Deployment(name=deployment.id, blueprint=blueprint)
                blueprint_deployment.associate_inputs_file()
                blueprint_deployment.save()

    return redirect('show-component', component_id)

@login_required
@require_GET
def unlink(request, component_id):
    component = models.Component.objects.get(id=component_id)
    blueprint = component.blueprint
    blueprint.component = None
    blueprint.remove_downloaded_files()
    blueprint.save()
    return redirect('show-component', component_id)


# Pushes locally stored blueprint to management server
#   Transforms 'local' blueprint into remote one
#   Uploaded blueprint is then modified according to new blueprint values from Cloudify REST client
# @login_required
# @require_GET
# def push_local_blueprint(request, blueprint_name):
#     b = models.Blueprint.objects.get(name=blueprint_name)
#
#     try:
#         blueprint = DashboardConfig.restClient.upload_blueprint(b.component.name, blueprint_name, b.local_filename)
#         if blueprint:
#             b.uploaded = True
#             b.created_at = blueprint.created_at
#             b.plan = blueprint.plan
#             b.save()
#             messages.success(request,
#                              "Local blueprint " + blueprint_name + " was successfully uploaded to Cloudify management server.")
#     # An error occurred when uploading blueprint to management server, display it
#     except Exception, e:
#         messages.error(request, str(e))
#
#     return redirect('get-blueprint', blueprint_name, 'info')

# Deletes a blueprint from database
#   If blueprint was uploaded to management server, deletes it from there
#   Also deletes the .yaml blueprint file associated with this blueprint, if it exists
@login_required
@require_GET
def delete(request, blueprint_name):
    blueprint = models.Blueprint.objects.get(name=blueprint_name)
    deployment = models.Deployment.objects.get(blueprint=blueprint)
    blueprint_folder = blueprint.get_folder_path()
    component_id = blueprint.component.id
    try:
        blueprint.component.manager.get_client().blueprints.delete(blueprint_name)
        if blueprint.local_filename:
            shutil.rmtree(blueprint_folder)

        blueprint.component = None  # Unbind from current component
        deployment.delete()  # Delete corresponding deployment
        blueprint.delete()  # Delete from database
        messages.success(request, "All files successfully deleted")
    except Exception, e:
        messages.error(request, str(e))

    return redirect('show-component', component_id)
