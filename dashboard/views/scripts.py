import os
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect, get_object_or_404, render
from django.views.decorators.http import require_POST, require_GET
from .. import models, forms, utils


@login_required
@require_GET
def get_script(request, script_id):
    script = get_object_or_404(models.Script, id=script_id)
    return render(request, 'dashboard/scripts.html', {"script": script, "blueprint": script.blueprint})


# @login_required
# @require_GET
# def get_script_candidates(request, blueprint_name):
#     blueprint = get_object_or_404(models.Blueprint, name=blueprint_name)
#
#     script_candidates = utils.get_candidates(blueprint, "script")
#
#     return render(request, 'dashboard/scriptCandidates.html', {"blueprint": blueprint, "scriptCandidates" : script_candidates})

def assign_scripts(request, blueprint_name, filename):
    blueprint = get_object_or_404(models.Blueprint, name=blueprint_name)

    s = models.Script(name=filename, blueprint=blueprint)
    s.save()
    messages.success(request, 'Script ' + s.name + ' successfully assigned.')

    return redirect('get-blueprint', blueprint_name, 'blueprint')


@login_required
@require_POST
def create_script(request, blueprint_name):
    b = get_object_or_404(models.Blueprint, name=blueprint_name)

    script_form = forms.ScriptUploadForm(request.POST, request.FILES)
    if script_form.is_valid():
        script_file = request.FILES['file']
        script_name = script_file.name
        s = models.Script(name=script_name, blueprint=b)
        s.save()
        utils.save_uploaded_file(script_file, b.application.name, b.name, script_name, script=True)
        messages.success(request, 'Successfully uploaded script ' + s.name)
    else:
        messages.error(request, script_form.errors)

    return redirect('get-blueprint', blueprint_name, 'info')


@login_required
@require_GET
def delete_script(request, script_id):
    s = models.Script.objects.get(id=script_id)
    blueprint_name = s.blueprint.name

    try:
        os.remove(s.get_path())
        s.delete()
        messages.success(request, "Script '" + s.name + ' successfully deleted.')
    except Exception, e:
        messages.error(request, str(e))

    return redirect('get-blueprint', blueprint_name, 'info')
