import datetime
import json
import logging

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.views.decorators.http import require_GET, require_POST

from .. import forms, models

logger = logging.getLogger(__name__)


@login_required
@require_GET
def index(request, reset=0):
    if reset or 'step' not in request.session:
        request.session['step'] = 1
        if 'domain' in request.session:
            del request.session['domain']
        if 'manager' in request.session:
            del request.session['manager']
    else:
        if request.session['step'] == 1:
            return redirect('domain-summary')
        elif request.session['step'] == 2:
            return redirect('connect-manager', request.session['manager'].id)
        elif request.session['step'] == 3:
            return redirect('show-component', request.session['component'].id)

    return redirect('domain-summary')

@login_required
@require_GET
def domain_summary(request):
    params = {'summary_trusted': get_domain_data(1),
              'summary_untrusted': get_domain_data(2)}
    return render(request, 'dashboard/domain/domain.html', params)


def get_domain_data(domain_type):
    domain = models.Domain.objects.get(id=domain_type)
    managers = domain.manager_set.all()
    providers = domain.get_cloud_providers()

    params = {'form': forms.MgmtServerIPForm(),
              'domain': domain,
              'managers': managers,
              'providers': json.dumps(providers)}
    return params


@login_required
@require_POST
def create_manager(request, domain_id):
    form = forms.MgmtServerIPForm(request.POST)

    if form.is_valid():
        manager = form.save()
        manager.domain = models.Domain.objects.get(id=domain_id)
        manager.save()
    else:
        messages.error(request, form.errors)

    return redirect('index')
    #return redirect('set-domain', request.session['domain_id'])


@login_required
@require_GET
def connect_manager(request, manager_id):
    # If you were previously connected to another manager, disconnect it first
    # if request.session.has_key('manager_ip'):
    #    disconnect_manager(request)
    selected_manager = models.Manager.objects.get(id=manager_id)
    selected_manager.last_used = datetime.datetime.now()
    selected_manager.save()

    # DashboardConfig.restClient.setManager(selected_manager.ip, selected_manager.ssl)
    request.session['domain'] = selected_manager.domain
    request.session["manager"] = selected_manager
    request.session["step"] = 2

    components = selected_manager.component_set.all()
    for component in components:
        component.refresh_status() # For showing updated deployment status

    params = {'manager': selected_manager,
              'components': components,
              'component_form': forms.ComponentForm()}

    return render(request, 'dashboard/component/components.html', params)


@login_required
@require_GET
def disconnect_manager(request):
    del request.session['manager']
    return redirect('index')


@login_required
@require_GET
def delete_manager(request, manager_id):
    manager = models.Manager.objects.get(id=manager_id)
    for component in manager.component_set.all():
        component.cleanup()
        component.delete()

    manager.delete()
    return redirect('index')
