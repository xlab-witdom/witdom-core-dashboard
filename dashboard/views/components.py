import os
from django.shortcuts import redirect, render
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_GET, require_POST
from django.contrib import messages
from .. import forms, models, synchronize


@login_required
@require_GET
def show(request, component_id):
    component = models.Component.objects.get(id=component_id)
    component_blueprint = models.Blueprint.objects.filter(component=component).first()
    request.session['step'] = 3
    request.session['component'] = component

    if component_blueprint is not None:
        return redirect('list-deployments', component_id)
    manager = component.manager
    synchronize.sync_database(manager.get_client())
    all_blueprints = models.Blueprint.objects.all()
    available_blueprints = [b for b in all_blueprints if b.component is None]

    params = {'blueprint_form': forms.BlueprintForm(),
              'component': component,
              'blueprint': component_blueprint,
              'blueprints': available_blueprints
              }

    return render(request, 'dashboard/component/componentDetails.html', params)


@login_required
@require_POST
def create(request):
    form = forms.ComponentForm(request.POST)

    if form.is_valid():
        app = form.save()  # Save to database
        manager_id = request.session["manager"].id
        manager = models.Manager.objects.get(id=manager_id)
        app.manager = manager
        app.save()
        # TODO
        os.makedirs('uploads/' + app.name)  # Create directory for this app within uploads folder
    else:
        messages.error(request, form.errors)

    return redirect('index')


# Deletes application entry without modification of related deployments, executions and blueprints either in DB or on management server
@login_required
@require_GET
def delete(request, component_id):
    component = models.Component.objects.get(id=component_id)
    component.cleanup()
    component.delete()
    request.session['step'] = 2
    if request.session.has_key('component'):
        del request.session['component']
    return redirect('index')
