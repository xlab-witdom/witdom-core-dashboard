import logging
import os
import shutil
import tarfile
import time
import utils
import yaml

from django.conf import settings
from django.db import models
from itsdangerous import base64_encode
from cloudify_rest_client.executions import Execution

logger = logging.getLogger(__name__)

INSTALL = 'install'
UNINSTALL = 'uninstall'
EXECUTE = 'execute_operation'

WORKFLOWS = (
    (INSTALL, 'Install'),
    (UNINSTALL, 'Uninstall'),
    (EXECUTE, 'Execute operation')
)


#    workflow_id = models.CharField(max_length=20, choices=WORKFLOWS, default=INSTALL, null=True)

class Domain(models.Model):
    trusted = models.BooleanField(default=True)

    AWS = 'Amazon Web Services'
    OS = 'OpenStack'
    CS = "CloudStack"
    VMW = "VMWare"

    PUBLIC_CLOUD_PROVIDERS = (
        (AWS, 'Amazon Web Services'),
    )

    PRIVATE_CLOUD_PROVIDERS = (
        (OS, 'OpenStack'),
        (CS, 'CloudStack'),
        (VMW, 'VMWare'),
    )
    PROVIDERS = PUBLIC_CLOUD_PROVIDERS + PRIVATE_CLOUD_PROVIDERS

    def get_name(self):
        return "trusted" if self.trusted else "untrusted"

    def get_cloud_providers(self):
        if self.trusted:
            return self.PRIVATE_CLOUD_PROVIDERS
        return self.PUBLIC_CLOUD_PROVIDERS


class Manager(models.Model):
    domain = models.ForeignKey(Domain, null=True)
    provider = models.CharField(max_length=20, default=Domain.OS, choices=Domain.PROVIDERS, null=True)

    ip = models.CharField(max_length=20, unique=False) # Just for the sake of review (to simulate manager in the untrusted domain)
    connected = models.BooleanField(default=False)
    authenticated = models.BooleanField(default=False)
    ssl = models.BooleanField(default=False)
    last_used = models.DateTimeField(null=True)

    def __unicode__(self):
        return u'%s - %s, requires authentication=%s' % (self.ip, self.provider, self.authenticated)

    def get_protocol(self):
        return "https" if self.ssl else "http"

    def get_port(self):
        return 443 if self.ssl else 80

    # Gets managers status with the help of cloudify's rest client
    def get_status(self):
        from cloudify_rest_client.exceptions import UserUnauthorizedError
        from requests import ConnectionError

        try:
            client = self.get_client()
            return client.manager.get_status().get('status')
        except ConnectionError:
            return "connection failed"
        except UserUnauthorizedError:
            client = self.get_authenticated_client()
            try:
                status = client.manager.get_status().get('status')
                self.authenticated = True
                self.save()
                return status
            except Exception as e:
                return str(e)
        except Exception as e:
            return str(e)

    def get_client(self):
        from cloudify_rest_client import CloudifyClient
        if self.authenticated:
            return self.get_authenticated_client()
        return CloudifyClient(host=self.ip, protocol=self.get_protocol(), port=self.get_port(), trust_all=True)

    def get_authenticated_client(self):
        from cloudify_rest_client import CloudifyClient
        headers = self.get_headers()
        return CloudifyClient(host=self.ip, protocol=self.get_protocol(), port=self.get_port(), trust_all=True,
                              headers=headers)

    def get_headers(self):
        authentication = self.get_authentication_token_header()
        authorization = self.get_authorization_header()
        return dict(authentication.items() + authorization.items())

    def get_authorization_header(self):
        # Read environment variables to get credentials
        username = os.environ.get(settings.CFY_USERNAME, False)
        password = os.environ.get(settings.CFY_PASSWORD, False)
        credentials = '{0}:{1}'.format(username, password)
        return {'Authorization': 'Basic ' + base64_encode(credentials)}

    def get_authentication_token_header(self):
        import uuid
        token = uuid.uuid4().hex
        return {'Authentication-Token': token}

# Represents application to be deployed, for instance Crypton
class Component(models.Model):
    WITDOM_COMPONENT_TYPES = (
        ('PC', 'Protection component'),
        ('PO', 'Protection Orchestrator'),
        ('B', 'Broker'),
        ('S', 'Service'),
        ('DB', 'Storage'),
        ('IAM', 'Identity and Access Management'),
        ('KM', 'Key manager')
    )

    NONE = 0
    DEPLOYING = 1
    SUCCESS = 2
    FAILURE = 3
    UNDEPLOYING = 4

    STATUSES = (
        (NONE, 'not deployed'),
        (DEPLOYING, 'deploy in progress'),
        (SUCCESS, 'successfuly deployed'),
        (FAILURE, 'deploy failed'),
        (UNDEPLOYING, 'undeploy in progress'),
    )

    name = models.CharField(max_length=100, blank=False) #, unique=True
    type = models.CharField(max_length=50, default='S', choices=WITDOM_COMPONENT_TYPES, null=False)
    provider = models.CharField(max_length=30, blank=False, null=True)
    description = models.CharField(default=None, null=True, max_length=500, blank=True)
    deployed = models.BooleanField(default=False)
    manager = models.ForeignKey(Manager, null=True)
    status = models.IntegerField(choices=STATUSES, default=NONE)

    def __unicode__(self):
        return u'%s' % self.name

    #def get_status_tag(self):
    #    return "deployed" if self.deployed else "not deployed"

    def refresh_status(self):
        from cloudify_rest_client.exceptions import CloudifyClientError
        self.status = self.NONE
        if hasattr(self, 'blueprint'):
            if self.blueprint.deployment_set.count():
                deployment = self.blueprint.deployment_set.first()
                try:
                    deployment_status = deployment.get_status()
                    if deployment_status == 0:
                        self.status = self.DEPLOYING
                    elif deployment_status == 1:
                        self.status = self.SUCCESS
                    elif deployment_status == 2:
                        self.status = self.FAILURE
                    elif deployment_status == 4:
                        self.status = self.UNDEPLOYING
                except CloudifyClientError as e:
                    pass # deployment doesn't exist on Cloudify manager anymore
        self.save()

    def get_status(self):
        self.refresh_status()
        return self.STATUSES[self.status][0]

    def get_status_str(self):
        return self.STATUSES[self.status][1]

    # Deletes all blueprint related folders associated with this app
    def cleanup(self):
        try:
            shutil.rmtree('uploads/' + self.name)
            self.blueprint.remove_downloaded_files()
        except Blueprint.DoesNotExist:
            logger.info("Tried to remove downloaded blutprint, but did not exist")
        except Exception as e:
            logger.info("Exception ocurred when deleting component %s (removing uploads folder)", self.name)

class Blueprint(models.Model):
    name = models.CharField(max_length=100, blank=False, unique=True, default="")
    created_at = models.DateTimeField(null=True)
    plan = models.CharField(max_length=2000, null=True)
    # Associated with blueprints originating from workstation
    local_filename = models.CharField(max_length=100,
                                      null=True)  # Name of the blueprint file which is stored locally
    local_filepath = models.CharField(max_length=300,
                                      null=True)  # Path (including local_filename) at which blueprint file is accessible
    uploaded = models.BooleanField(default=True)

    # Associated with blueprints originating from the Cloudify management server
    downloaded = models.BooleanField(default=False)
    component = models.OneToOneField(Component,
                                     null=True)  # Can exist on the management server, but is unassigned to the application

    def __unicode__(self):
        return u'%s' % self.name

    def get_folder_path(self):
        if self.downloaded:
            return 'downloads/{}/{}'.format(self.name, self.name)
        else:
            return 'uploads/{}/{}'.format(self.component.name, self.name)

    def get_file_path(self):
        if self.local_filepath:
            folder = self.local_filepath  # Remote blueprint was not uploaded from this dashboard and does not obey our hierarchy
        else:
            folder = self.get_folder_path()  # Remote blueprint was uploaded from this dashboard

        return folder + '/' + self.local_filename

    def display_yaml_file(self):
        return open(self.get_file_path()).read()

    def get_extract_path(self):
        path = 'downloads/' + self.name
        if not os.path.exists(path):
            os.makedirs(path)
        return path

    def rename_blueprint_folder(self, path):
        for file in os.listdir(path):
            filepath = os.path.join(path, file)
            if os.path.isdir(filepath):
                os.rename(os.path.join(path, file), os.path.join(path, self.name))

    def extract_archive(self):
        extract_path = self.get_extract_path()
        archive = "{}.tar.gz".format(extract_path)
        tar = tarfile.open(archive)
        tar.extractall(path=extract_path)
        self.rename_blueprint_folder(extract_path)
        tar.close()
        os.remove(archive)

    def download_remote_files(self):
        extract_path = self.get_extract_path()
        cloudify_client = self.component.manager.get_client()
        cloudify_client.blueprints.download(self.name, output_file="{}.tar.gz".format(extract_path))
        self.extract_archive()
        self.downloaded = True


    def remove_downloaded_files(self):
        shutil.rmtree(self.get_extract_path())


    def associate_blueprint_file(self):
        folder = self.get_folder_path()
        blueprint_file = None
        for file in os.listdir(folder):
            if (file.endswith(".yaml") or file.endswith(".yml")) and not "inputs" in file:
                blueprint_file = file
                break
        if blueprint_file:
            self.local_filename = blueprint_file

      #new_deployment.save()
    # script_names = utils.get_script_names(b)
    # for script_name in script_names:
    #     s = models.Script(name=script_name, blueprint=b)
    #     s.save()
    #

    def associate_files(self):
        self.associate_blueprint_file()
        #self.associate_blueprint_inputs_file()
        #self.associate_scripts()

# When uploading application blueprint to Cloudify management server, the whole folder holding the blueprint file gets uploaded
#  This includes optional scripts
class Script(models.Model):
    blueprint = models.ForeignKey(Blueprint, null=True)
    name = models.CharField(max_length=100, null=True)  # must include suffix as well, for instance 'configure.sh'
    filepath = models.CharField(max_length=300, null=True)

    def __unicode__(self):
        return u'%s' % self.name

    def read(self):
        return open(self.get_path()).read()

    def get_path(self):
        if self.blueprint.downloaded and self.filepath:
            return self.filepath + "/" + self.name
        elif self.blueprint.downloaded:
            return 'downloads/' + self.blueprint.name + '/' + self.blueprint.name + '/' + "scripts/" + self.name
        else:
            return 'uploads/' + self.blueprint.component.name + '/' + self.blueprint.name + "/scripts/" + self.name


class Deployment(models.Model):
    name = models.CharField(max_length=200)
    blueprint = models.ForeignKey(Blueprint, null=True)
    # workflows = models.CharField(max_length=2000, null=True)
    uploaded = models.BooleanField(default=True)
    undeploying = models.BooleanField(default=False)
    local_inputs_filename = models.CharField(max_length=100, null=True)

    PROGRESS = 0
    SUCCESS = 1
    FAIL = 2
    NONE = 3
    UNDEPLOYING = 4

    STATUSES = [PROGRESS, SUCCESS, FAIL, NONE, UNDEPLOYING]

    def read_inputs(self):
        if self.local_inputs_filename:
            return open(self.get_inputs_path()).read()

    def get_inputs_path(self):
        return "{}/{}".format(self.get_folder_path(), self.local_inputs_filename)

    def get_folder_path(self):
        if self.blueprint.downloaded:
            return 'downloads/{}/{}'.format(self.blueprint.name, self.blueprint.name)
        else:
            return 'uploads/{}/{}'.format(self.blueprint.component.name, self.blueprint.name)

    def associate_inputs_file(self):
        folder = self.get_folder_path()
        inputs_file = None
        for file in os.listdir(folder):
            if (file.endswith(".yaml") or file.endswith(".yml")) and "inputs" in file:
                inputs_file = file
                break
        if inputs_file:
            self.local_inputs_filename = inputs_file

    def upload_to_manager(self):
        logger.info("Deployment: uploading to manager")
        f = open(self.get_inputs_path(), 'r')
        inputs_dict = yaml.load(f.read())
        f.close()
        cloudify_client = self.blueprint.component.manager.get_client()
        cloudify_client.deployments.create(self.blueprint.name, self.name, inputs_dict)

    def run(self):
        logger.info("Deployment: running")
        cloudify_client = self.blueprint.component.manager.get_client()
        initial_execution = cloudify_client.executions.list(self.name)[0]
        while initial_execution.status not in Execution.END_STATES:  # Wait for deployment environment creation execution to finish
            time.sleep(5)
            initial_execution = cloudify_client.executions.list(self.name)[0]
            logger.info("Waiting for initial execution to finished...")
        cloudify_client.executions.start(self.name, "install")

    def uninstall_and_wait(self, cloudify_client):
        self.status = self.UNDEPLOYING
        self.save()
        while True:
            try:
                uninstall = cloudify_client.executions.start(self.name, "uninstall")
                logger.info("Deployment: Starting uninstall workflow")
                while cloudify_client.executions.get(uninstall.id, _include=["status"]).status not in Execution.END_STATES:
                    logger.info("Deployment: Waiting for uninstall workflow to finish")
                    time.sleep(3)
                break
            except Exception as e:
                logger.info("Waiting before uninstall procedure can start - " + str(e))
                time.sleep(3)
        logger.info("Deployment: Uninstall workflow finished")

    def cancel_and_wait(self, execution, cloudify_client):
        logger.info("Forcing cancel of workflow " + execution.workflow_id + " - " + str(execution.id))
        cloudify_client.executions.cancel(execution.id, force=True)
        while cloudify_client.executions.get(execution.id, _include=["status"]).status not in Execution.END_STATES:
            logger.info("Waiting for cancel of " + execution.workflow_id + " workflow")
            time.sleep(3)
        logger.info(execution.workflow_id + " workflow cancelled")

    def cleanup(self, executions, cloudify_client):
        last_execution = executions[-1] #0 sort order??
        logger.info("Deployment: Last execution: " + str(last_execution.id) + " " + str(last_execution.workflow_id))
        if last_execution.status not in Execution.END_STATES:
            self.cancel_and_wait(last_execution, cloudify_client)
            if last_execution.workflow_id == "install":
               self.uninstall_and_wait(cloudify_client)

    def wait_for_executions_to_finish(self, cloudify_client):
        while True:
            executions = cloudify_client.executions.list(self.name, _sort="created_at", include_system_workflows=True)
            all_executions_ended = True
            for execution in executions:
                if execution.status not in Execution.END_STATES:
                    self.cancel_and_wait(execution, cloudify_client)
                    all_executions_ended = False
                    logger.info("Deployment: All executions have not ended yet, waiting and trying again")
                    time.sleep(3)
                    break
            if all_executions_ended:
                logger.info("Deployment: All executions ended")
                break

    def delete_from_manager(self):
        logger.info("Deployment: Starting deployment deletion procedure")
        cloudify_client = self.blueprint.component.manager.get_client()
        executions = cloudify_client.executions.list(self.name, _sort="created_at")
        for execution in executions:
            logger.info("{} - {}".format(execution.created_at, execution.id))
        self.cleanup(executions, cloudify_client)
        self.wait_for_executions_to_finish(cloudify_client)
        cloudify_client.deployments.delete(self.name, ignore_live_nodes=True)
        logger.info("Deployment: Successfully deleted deployment")


    def is_deployed(self):
        status = self.get_status()
        return status == self.SUCCESS

    def get_status(self):
        cloudify_client = self.blueprint.component.manager.get_client()
        executions = cloudify_client.executions.list(deployment_id=self.name, _include=["workflow_id", "status"], _sort="created_at")

        if len(executions):
            latest_execution = executions[-1] #0
            if latest_execution.workflow_id == "install":
                if latest_execution.status == "terminated":
                    return self.SUCCESS
                elif latest_execution.status == "failed":
                    return self.FAIL
                elif latest_execution.status == "pending" or latest_execution.status == "started":
                    return self.PROGRESS
            elif latest_execution.workflow_id == "create_deployment_environment" and latest_execution.status == "started":
                return self.PROGRESS
            elif self.undeploying is True or latest_execution.workflow_id == "delete_deployment_environment" and latest_execution.status == "started":
                return self.UNDEPLOYING
        return self.NONE

# Execution parameters
class Parameter(models.Model):
    OPERATION = 'operation'
    TYPE_NAMES = 'type_names'
    NODE_IDS = 'node_ids'
    NODE_INSTANCE_IDS = 'node_instance_ids'
    START = 'start'
    STOP = 'stop'
    CONFIGURE = 'configure'

    WORKFLOW_PARAMS = (
        (TYPE_NAMES, 'Type names'),
        (NODE_IDS, 'Node IDs'),
        (NODE_INSTANCE_IDS, 'Node instance IDs')
    )

    OPERATIONS = (
        (START, 'Start'),
        (STOP, 'Stop'),
        (CONFIGURE, 'Configure')
    )

    key = models.CharField(max_length=50, choices=WORKFLOW_PARAMS)
    value = models.CharField(max_length=200, null=True)
