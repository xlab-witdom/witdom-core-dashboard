from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.core import validators
from django.forms.widgets import Select, TextInput

from models import Component, Parameter, Blueprint, Manager, Domain


def validate_file_extension(value):
    if not value.name.endswith('.yaml'):
        raise validators.ValidationError(
            'The file you are trying to upload is not in YAML format. Please choose YAML file.')


# Form for registration of new user
#   Extends django provided UserCreationForm with user's first and last name
class ExtendedUserCreationForm(UserCreationForm):
    first_name = forms.CharField(max_length=100)
    last_name = forms.CharField(max_length=100)

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'username', 'password1', 'password2']

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password1'])
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.is_active = True

        if commit:
            user.save()

        return user


class MgmtServerIPForm(forms.ModelForm):
    class Meta:
        model = Manager
        fields = ['provider', 'ip', 'ssl']

        labels = {
            'ssl': 'Uses SSL',
            'ip': 'IP',
        }

    #def __init__(self, *args, **kwargs):
    #    super(MgmtServerIPForm, self).__init__(*args, **kwargs)
    #    domain_id = args[0]
    #    if domain_id == 1:
    #        providers = Domain.PRIVATE_CLOUD_PROVIDERS
    #    else:
    #        providers = Domain.PUBLIC_CLOUD_PROVIDERS
        #self.fields['provider'] = forms.CharField(label="Provider", widget=forms.Select(choices=providers))
        #self.fields['ip'] = forms.CharField(label="IP", widget=forms.TextInput())
    #    self.fields['ssl'] = forms.BooleanField(label="Uses SSL", widget=forms.TextInput())
        #self.fields['domain'] = forms.ChoiceField(widget=forms.Select(choices=Domain.objects.all()))

class ComponentForm(forms.ModelForm):
    class Meta:
        model = Component
        fields = ['name', 'type', 'provider', 'description']

    def __init__(self, *args, **kwargs):
        super(ComponentForm, self).__init__(*args, **kwargs)
        self.fields['name'] = forms.CharField(label="Name",
                                              widget=forms.TextInput(attrs={'placeholder': 'Component name'}))
        self.fields['type'] = forms.CharField(label="Category",
                                              widget=forms.Select(choices=Component.WITDOM_COMPONENT_TYPES))
        self.fields['author'] = forms.CharField(label="Author", widget=forms.TextInput(
            attrs={'placeholder': 'Author that contributed this component'}))
        self.fields['description'] = forms.CharField(required=False, label="Description", widget=forms.Textarea(
            attrs={'placeholder': 'Optional description of component', 'rows': 3}))


class ParameterForm(forms.ModelForm):
    class Meta:
        model = Parameter
        fields = ['key', 'value']

    extra_params_count = forms.CharField(widget=forms.HiddenInput())

    def __init__(self, *args, **kwargs):
        extra_params = kwargs.pop('extra', 0)
        super(ParameterForm, self).__init__(*args, **kwargs)

        self.fields['key'].widget = TextInput()
        self.fields['key'].widget.attrs['readonly'] = True
        self.fields['key'].widget.attrs['value'] = 'operation'
        self.fields['key'].widget.attrs['class'] = 'form-control'

        self.fields['value'].widget = Select(choices=Parameter.OPERATIONS)
        self.fields['value'].widget.attrs['class'] = 'form-control'
        self.fields['extra_params_count'].initial = extra_params

        for index in range(int(extra_params)):
            self.fields['extra_key_{index}'.format(index=index)] = forms.CharField(
                widget=forms.Select(choices=Parameter.WORKFLOW_PARAMS))
            self.fields['extra_value_{index}'.format(index=index)] = forms.CharField(widget=forms.TextInput())
            self.fields['extra_key_{index}'.format(index=index)].widget.attrs['class'] = 'form-control'
            self.fields['extra_value_{index}'.format(index=index)].widget.attrs['class'] = 'form-control'


class BlueprintForm(forms.ModelForm):
    class Meta:
        model = Blueprint
        fields = ['name']

    blueprint_file = forms.FileField(validators=[validate_file_extension])
    blueprint_inputs_file = forms.FileField(validators=[validate_file_extension])
    script_file = forms.FileField(required=False)

# class ExecutionForm(forms.ModelForm):
#     class Meta:
#         model = Execution
#         fields = ['workflow_id']
#
#     def __init__(self, *args, **kwargs):
#         default_choice = kwargs.pop('default', Execution.INSTALL)
#         super(ExecutionForm, self).__init__(*args, **kwargs)
#         self.fields['workflow_id'].initial = default_choice

# class ScriptUploadForm(forms.Form):
#    file = forms.FileField()

# class DeploymentForm(forms.ModelForm):
#     class Meta:
#         model = Deployment
#         fields = ['deployment_id', 'local_inputs_filename']
#
#     file = forms.FileField(validators=[validate_file_extension])
