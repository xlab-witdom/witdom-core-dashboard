from django.contrib import admin
from . import models

admin.site.register(models.Component)
admin.site.register(models.Blueprint)
admin.site.register(models.Deployment)
admin.site.register(models.Parameter)
admin.site.register(models.Script)
admin.site.register(models.Manager)
