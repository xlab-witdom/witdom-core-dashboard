import logging
import sys

from . import utils
from models import Blueprint, Deployment, Parameter

INCLUDED_KEYS = ['floating_ip_address', 'external_name', 'chef_attributes']


def sync_database(client):
    blueprints = client.blueprints.list()

    # Delete blueprint records that are no longer on manager server
    for blueprint in Blueprint.objects.all():
        if blueprint.uploaded:
            found = False
            for remote_blueprint in blueprints:
                if blueprint.name == remote_blueprint.id:
                    found = True
                    break
            if not found:
                blueprint.application = None
                blueprint.delete()

    for blueprint in blueprints:
        # Save all blueprints that are on the management server, but not already in the databse
        if not Blueprint.objects.filter(name=blueprint.id).exists():
            b = Blueprint(name=blueprint.id)
            b.plan = blueprint.plan
            b.created_at = utils.convert_datetime(blueprint.created_at)
            b.save()
        else:
            b = Blueprint.objects.get(name=blueprint.id)

        # handle_deployments(client, b)
        b.save()


def handle_deployments(client, blueprint):
    deployments = client.deployments.list(blueprint_id=blueprint.name)

    # Delete deployment records that are no longer on manager server
    for deployment in Deployment.objects.all():
        if deployment.uploaded:
            found = False
            for remote_deployment in deployments:
                if deployment.deployment_id == remote_deployment.id:
                    found = True
                    break
            if not found:
                deployment.blueprint = None
                deployment.delete()

    for deployment in deployments:
        if not Deployment.objects.filter(deployment_id=deployment.id).exists():
            d = Deployment(deployment_id=deployment.id)
            d.blueprint = blueprint
            d.inputs = deployment.inputs
            d.outputs = deployment.outputs
            d.save()
        else:
            d = Deployment.objects.get(deployment_id=deployment.id)

        d.save()


def create_execution_params(e, execution):
    for k, v in execution.parameters.iteritems():
        p = Parameter(key=k, value=v, execution=e)
        p.save()
