from django.conf.urls import url
from . import views

urlpatterns = [
    # Manager-related actions
    url(r'^index/(?P<reset>\d+)?$', views.managers.index,
        name='index'),
    url(r'^index/$', views.managers.index,
        name='index'),
    url(r'^domains$', views.managers.domain_summary,
        name='domain-summary'),
    # url(r'^domain/(?P<domain_type>\d+)/$', views.managers.set_domain,
    #     name='set-domain'),

    url(r'^manager/new/(?P<domain_id>[\S]+)$', views.managers.create_manager,
        name='create-manager'),
    url(r'^manager/(?P<manager_id>[\S]+)/$', views.managers.connect_manager,
        name='connect-manager'),
    url(r'^manager/disconnect/(?P<manager_id>[\S]+)/$', views.managers.disconnect_manager,
        name='disconnect-manager'),
    url(r'^manager/delete/(?P<manager_id>[\S]+)/delete$', views.managers.delete_manager,
        name='delete-manager'),

    # component-related actions
    url(r'^components/(?P<component_id>[\S]+)/deployments/$', views.deployments.list,
        name='list-deployments'),
    url(r'^components/(?P<component_id>[\S]+)/deployments/(?P<wait>[\S]+)/$', views.deployments.list,
        name='list-deployments-delayed'),
    url(r'^components/unlink/(?P<component_id>[\S]+)$', views.blueprints.unlink,
        name="unlink-blueprint"),
    url(r'^components/create/$', views.components.create,
        name='create-component'),
    url(r'^components/(?P<component_id>[\S]+)/delete/$', views.components.delete,
        name='delete-component'),

    # Blueprint-related actions
    url(r'^components/(?P<component_id>[\S]+)/blueprints/create/$', views.blueprints.create,
        name='create-blueprint'),
    url(r'^components/(?P<component_id>[\S]+)/$', views.components.show,
        name='show-component'),
    url(r'^components/(?P<component_id>[\S]+)/blueprints/link/(?P<blueprint_id>[\S]+)$',
        views.blueprints.link,
        name='link-blueprint'),

    # url(r'^blueprints/push/(?P<blueprint_name>[\S]+)$', views.blueprints.push_local_blueprint, name='push-blueprint'),
    url(r'^blueprints/save/(?P<blueprint_name>[\S]+)$', views.blueprints.serve,
        name='serve-blueprint'),
    url(r'^blueprints/get/(?P<blueprint_name>[\S]+)/candidates/$', views.blueprints.get_candidates,
        name='get-blueprint-candidates'),
    url(r'^blueprints/get/(?P<blueprint_name>[\S]+)/candidates/assign/(?P<filename>[\S]+)$',
        views.blueprints.assign_blueprint_file,
        name='assign-blueprint-file'),

    url(r'^blueprints/files/(?P<blueprint_name>[\S]+)/$', views.blueprints.show_files,
        name='show-blueprint-files'),
    url(r'^blueprints/delete/(?P<blueprint_name>[\S]+)/$', views.blueprints.delete,
        name='delete-blueprint'),

    # Blueprint script-related actions
    url(r'^blueprints/(?P<blueprint_name>[\S]+)/scripts/create/$', views.scripts.create_script,
        name='create-script'),
    url(r'^blueprints/scripts/(?P<script_id>[\S]+)/delete/$', views.scripts.delete_script,
        name='delete-script'),
    url(r'^scripts/(\d+)/get/$', views.scripts.get_script,
        name='get-script'),

    url(r'^deployments/(?P<blueprint_id>[\S]+)/(?P<deployment_id>[\S]+)/delete/$', views.deployments.delete,
        name='delete-deployment'),
    url(r'^blueprints/(?P<blueprint_id>[\S]+)/run/$', views.deployments.start,
        name='run-deployment'),
    url(r'^blueprints/(?P<blueprint_name>[\S]+)/create/$', views.deployments.create,
        name='create-deployment'),
    url(r'^executions/(?P<deployment_name>[\S]+)/(?P<execution_id>[\S]+)$', views.executions.get_events,
        name='get-execution-events'),
    url(r'^deployments/(?P<deployment_id>[\S]+)/get/$', views.deployments.get,
        name='get-deployment'),


    # Authentication
    url(r'^login/$', 'django.contrib.auth.views.login', name='login'),
    url(r'^logout/$', 'django.contrib.auth.views.logout', {'next_page': '/login'}, name='logout'),
    url(r'^register/$', views.authentication.register_user, name='register'),

    url(r'^$', views.managers.index),
]
