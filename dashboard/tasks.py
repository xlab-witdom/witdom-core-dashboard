from djcelery import celery

@celery.task
def deploy(deployment):
    try:
        deployment.upload_to_manager()
        deployment.run()
    except Exception as e:
        print str(e)

@celery.task
def undeploy(deployment):
    try:
        deployment.delete_from_manager()
    except Exception as e:
        print str(e)