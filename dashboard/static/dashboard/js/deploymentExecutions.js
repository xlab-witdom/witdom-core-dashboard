$(document).ready(function () {

     var periodicTask;

    $(".btnShowLogs").click(function () {
        deploymentName = $("#deploymentName").val();
        executionData = $(this).parent().parent().attr('id').split("_");
        executionId = executionData[0];
        executionStatus = executionData[1];

        url = "/executions/" + deploymentName + "/" + executionId;

        /* If this is an active execution, refresh logs periodically - every 3 seconds */
        if (executionStatus == "pending" || executionStatus == "started" || executionStatus == "force_cancelling") {
            periodicTask = setInterval(function(){
                queryLogs(url, executionId);
            }, 3000);
        } else {
             /* no need to periodically refresh logs, just call once */
             queryLogs(url, executionId);
        }
    });

    /* When user closes modal, stop periodic task, if it was set */
    $('.logsModal').on('hidden.bs.modal', function () {
        clearInterval(periodicTask);
    });

    /* Executes  ajax get request for a given URL to retrieve execution logs */
    function queryLogs(url, executionId) {
        elementId = "#retrievedLogs-" + executionId;
        console.log(elementId);
        $.get(url, function(data) {
            $(elementId).html(data);
        })
        .fail(function() {
            $(elementId).html("Error retrieving execution logs");
        });
    }

});
