$(function() {
  $("#id_script_file").change(function () {
    var fileName = $(this).val().split(/(\\|\/)/g).pop();
      newScriptsList = $("#scriptsList").text() + " " + fileName;
      $("#scriptsList").text(newScriptsList);
      $("#scriptsFeedbackDiv").show();
      //$("#addScriptSpan").text("Add another script");
  });

  $("#id_blueprint_file").change(function (){
     var fileName = $(this).val().split(/(\\|\/)/g).pop();
     $("#filename_blueprint").html(fileName);
   });

  $("#id_blueprint_inputs_file").change(function(){
    var fileName = $(this).val().split(/(\\|\/)/g).pop();
    $("#filename_blueprint_inputs").html(fileName);
  });
});