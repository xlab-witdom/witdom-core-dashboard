import datetime
import os
import re


# from config import DashboardConfig

# Saves a file to hierarchy within uploads folder
def save_uploaded_file(file, application_name, blueprint_name, filename, **kwargs):
    path = 'uploads/' + application_name + "/" + blueprint_name

    if 'script' in kwargs:
        path = path + '/scripts'

    if not os.path.exists(path):
        os.makedirs(path)

    path += ("/" + filename)
    with open(path, 'wb+') as destination:
        for chunk in file.chunks():
            destination.write(chunk)
    return path


def zipdir(directory, zf):
    for dirname, subdirs, files in os.walk(directory):
        zf.write(dirname)
        for filename in files:
            zf.write(os.path.join(dirname, filename))

def get_scripts(blueprint):
    if os.path.exists():
        for script_file in os.listdir():
            print script_file
            # script = Script(blueprint=blueprint, name=script_file)


def find_script_names_in_blueprint(blueprint):
    script_filename_regex = r'\w+\.sh'
    pattern = re.compile(script_filename_regex)

    script_names = re.findall(pattern, open(blueprint.get_file_path()).read())
    script_paths = []

    unique_script_names = set()
    for script in script_names:
        unique_script_names.add(script)
        path = get_candidate_path(blueprint.name, script)
        script_paths.append(path)

    return unique_script_names, script_paths


def get_script_names(blueprint):
    dl_folder = blueprint.get_folder_path()
    script_folder = dl_folder + "/scripts"
    scripts = []

    if os.path.exists(script_folder):
        for script_file in os.listdir(script_folder):
            scripts.append(script_file)

    return scripts


def get_candidate_path(blueprint_name, blueprint_filename):
    for dirpath, dirnames, filenames in os.walk('downloads/' + blueprint_name):
        if blueprint_filename in filenames:
            return dirpath


# Lists .yaml or .sh files that were uploaded with blueprint to Cloudify manager
def get_candidates(blueprint, type):
    candidates = []

    if type == "blueprint":
        suffix = ".yaml"
    elif type == "script":
        suffix = ".sh"

    for dirpath, dirnames, filenames in os.walk('downloads/' + blueprint.name):
        for filename in filenames:
            if filename.endswith(suffix):
                candidates.append(filename)

    return candidates


# Converts unicode representation of date and time obtained from Cloudify REST client
#    to a value appropriate for Django's DateTimeField used in models
def convert_datetime(datetime_str):
    return datetime.datetime.strptime(datetime_str, "%Y-%m-%d %H:%M:%S.%f")
