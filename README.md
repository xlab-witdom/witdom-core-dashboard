# WITDOM Cloudify Dashboard
WITDOM Dashboard is a Python web application running on top of  [Django](https://www.djangoproject.com/), a high-level Python web framework. Dashboard's purpose is to provide a web UI (User Interface) for administrative users tasked with the deploy of components/services to cloud infrastructure through [Cloudify](http://getcloudify.org/), a cloud automation and orchestration tool. WITDOM dashboard offers a subset of functionality of Cloudify's Command-Line Interface (CLI) (i.e. querying Cloudify manager server, uploading/deleting application blueprints, uploading/deleting deployments for a given blueprint, controlling execution workflows, reading deployment outputs etc.) with the help of [Cloudify's REST client for Python](http://cloudify-rest-client.readthedocs.io/en/3.3/). The free, open-soruce Cloudify edition does not come with a web UI. Instead, it is provided as a premium feature.

From the development perspective, our efforts so far were mainly focused on establishing a user-friendly interface for managing Cloudify deployments and gathering information about infrastructure deployed with Cloudify. However, it is our intention to extend the dashboard app with other (Cloudify unrelated) functionality important for WITDOM.

For a detailed overview of WITDOM dashboard please refer to _docs/DESCRIPTION.md_

## Project structure
* _docs_ - Structured documentation for this project (see below)
* _screenshots_ - Here you can find some screenshots of the dashboard's UI.
* _test-files_ - Files that can be used in the dashboard app for demonstration purposes. Contains Cloudify application blueprints in TOSCA .yaml format and their corresponding inputs.
* _dashboard_ - Core of our Django app 
* _witdom-platform_ - Django app configuration 

## Documentation
Below is a list of files where you can find documentation for this project. Except for this file, all documentation can be found in the _docs_ folder.
* **README** - this file: ToC, general description of the dashboard app
* **DESCRIPTION** - detailed description of the dashboard app
* **SPECIFICATIONS** - prerequisites for running & specifications for developing dashboard app
* **INFRASTRUCTURE** - an overview of our OpenStack infrastructure related to WITDOM Dashboard
* **SETUP** - instructions for running the project and setting up the environment
* **USAGE** - instructions for using the dashboard app
* **ROADMAP** - main references for future work
