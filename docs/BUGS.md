# Bugs to fix

* when deleting a blueprint that does not exist locally (was only linked from Cloudify Manager), we get an error (problem when deleting files)
* when manually uploading script before uploading local blueprint to manager: script name gets accounted for twice
