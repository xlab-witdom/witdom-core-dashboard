# WITDOM Dashboard description
This document contains an overview of some key aspects regarding the use of Cloudify in WITDOM Dashboard and WITDOM in general. Description of Cloudify unrelated dashboard functionality is also provided at the end of this document.

## WITDOM and Cloudify
##### How Cloudify stores infrastructural information
Cloudify's deployment model and runtime data are stored as **JSON** documents.

##### How to access these data
All the information regarding Cloudify service deployments and their corresponding infrastructure (represented in JSON format) are obtained via REST API, exposed by Cloudify manager. Manager's REST service is the access point of all clients to the management server.

No API keys are necesarry.

##### Cloudify Manager's REST API
Cloudify open-sourced two flavors of Cloudify REST client:

* Python REST Client (used by WITDOM-Dashboard app written in Python)
* NodeJS REST Client (will probably be used by WITDOM-Broker, which is a NodeJS application itself)


##### What data can we get about services/infrastructure deployed with Cloudify?
Data regarding application blueprints, their corresponding deployments and executions, deployments outputs (IPs, ports, VM instance names etc.). The latter are probably the easiest way to communicate information about apps/infrastructure deployed with Cloudify to clients.

##### How to interpret data obtained from Cloudify manager
The format of the data returned in JSON is well documented. By using either Python or NodeJS REST client, no explicit parsing is necesarry - the data are automatically returned in the form of arrays or dictionaries.

Additionally, it is possible to specify exactly what fields we want included in responses.

##### How to *securely* access data from Cloudify manager
*(This might be interesting for WITDOM components deployed in the untrusted domain)*

It is possible to authenticate all external requests to the REST service. However, Cloudify manager must be properly configured at the time of bootstrapping - this configuration is given in manager blueprint file. To secure REST services, Cloudify’s security framework uses the Flask-secuREST package.

There are 2 prerequisites that must be met for authenticated communication with Cloudify's REST service:

* **userstore** - storage (file/database) of approved users' unique identifiers, e.g. username/id/username-password combination/... Cloudify supports several userstore implementations (Flask-secuREST simple userstore may suffice). It is also possible to use LDAP.
* **authentication provider** - performs authentication. Multiple providers can be configured in order to support several authentication methods (e.g. password or token-based, Kerberos)

##### What is the easiest way to obtain information regarding deployment tasks (e.g. from WITDOM-Dashboard or WITDOM-Broker)?
All communication with Cloudify manager (which is the only entity capable of providing such info) can be obtained via REST API calls. However, how easy or how hard it is depends on the type of data client (dashboard/broker) needs. 

Some common runtime properties that can be easily obtained:

* internal IP address
* floating IP address
* port at which the deployed service is available
* ID, type and name of the resource

Fetching design-time properties (hard-coded in application blueprints) is also trivial.

### Integrating WITDOM-Broker, WITDOM-Dashboard and Cloudify

* As previously stated, WITDOM-Dashboard will use Cloudify's Python REST client with the goal of deploying applications with Cloudify, as well as displaying data related to Cloudify deployments.
* Since WITDOM-Broker will be implemented as a NodeJS application, it is expected that the integration with Cloudify REST client will be straightforward. Broker will be modified in order to make REST API calls to obtain deployment data from Cloudify manager. Other clients can be used to send requests to the REST service. An alternative to Python and NodeJS REST client is **curl** client, which could be used in case any other WITDOM mediator (e.g. protection orchestator) that is not implemented as a Python/NodeJS application needed Cloudify-related information directly from management server. It is too early to predict to which extent Cloudify-provided data can be used, not knowing exactly what data WITDOM-Broker expects from other WITDOM services/components. Furthermore, there are limitations regarding what data can be obtained about these services through Cloudify alone.
* WITDOM-Dashboard and WITDOM-Broker can both connect to the same Cloudify manager, each using their own REST client and each using obtained Cloudify-related data for their purposes. We do not anticipate the need for directly integrating WITDOM-Broker with WITDOM-Dashboard.

## WITDOM Dashboard's Cloudify unrelated features
At the moment, dashboard's already integrated Cloudify unrelated features include:

* __Authenticating dashboard users with their LDAP credentials__ - Dashboard app is connected to FreeIPA user directory, which means users from our central repository don't need to create new accounts only for the purpose of authenticating with WITDOM-Dashboard.
* A logical layer "above Cloudify" that allows creation of "applications". The dashboard app uses the notion of applications solely for the purpose of (logically) grouping blueprints. For instance, WITDOM-Dashboard allows us to create an application with name "E2EE" by company "XLAB" and entering the app's short description. We can then logically bind a set of Cloudify application blueprints to our newly created "E2EE" application. Without this logical grouping of blueprints into applications the dashboard would become a bit messy.

Future extensions and improvements include:

* __User authentication with Single-sign-on (SSO) enabled by SimpleSAMLphp app instead of directly connecting to LDAP user directory__ We want users from our LDAP directory (provided by FreeIPA) to authenticate with their existing credentials in WITDOM dashboard. The idea is to configure our dashboard app as a service provider (SP) for SimpleSAMLphp app. SimpleSAMLphp app is already configured to serve as identity provider (IDP) and is able to retrieve information about our users from LDAP directory.

 We already tried implementing this with the help of _djangosaml2_ library. Due to the library not being maintained anymore and incompatibility with newer versions of Django we were unable to correctly implement SSO functionality. The issues seem to originate from djangosaml2 xml parser incorrectly handling SAML attributes.
