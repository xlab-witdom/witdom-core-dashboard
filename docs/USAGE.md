# How to use WITDOM Dashboard

Below is a list of steps necessary to deploy simple infrastructure on OpenStack (WITDOM tenant) with our dashboard. Note that these steps correspond to the version of dashboard currently deployed on our web-server instance on OpenStack, which is somewhat outdated (it uses an older version of Cloudify and doesn't support authentication with LDAP).

1. Register a new user OR login with credentials u=admin, pw=admin (already generated)
2. Enter IP address of your Cloudify management server and create a new manager
3. Establish connection to manager of your choice by clicking ("Use manager at <ip_address>")
4. Create new application
5. Link any of the blueprints already on Cloudify management server to created application
6. OR/AND create new blueprint (ATM only upload works. Try uploading _test-simple-blueprint.yaml_ blueprint located in cloudifyDashboard-django folder)
7. Upload newly created blueprint to management server
8. Click on uploaded blueprint and create new deployment - choose arbitrary deployment identifier and arbitrary name for file holding inputs (without .yaml suffix). Select _test-simple-blueprint-inputs.yaml_ and submit form
9. Upload newly created deployment on Cloudify management server
10. Create new 'install' execution for created deployment. The app will show you the status of execution and you will see a list of nodes that were created as a result, as well as deployment outputs (if the application blueprint specified any).
11. To cleanup, create 'uninstall' execution, which will free newly created OpenStack resources

In addition to the above scenario, newest version of WITDOM Dashboard (which is ATM not deployed on OpenStack) supports authentication of users with LDAP backend (is connected to FreeIPA user directory) and establishment of HTTPS communication with Cloudify Manager.

