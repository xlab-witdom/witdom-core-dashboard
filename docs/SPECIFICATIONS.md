# WITDOM Dashboard Specifications

## Technical specifications
* Python version: **2.7**
* Django version: **1.8**
* Cloudify version: **3.4** (Cloudify manager and Cloudify's REST client)

## Prerequisites
* An up-and-running instance of Cloudify manager
* [RabbitMQ server](https://www.rabbitmq.com/) (`sudo apt-get install -y rabbitmq-server`)
