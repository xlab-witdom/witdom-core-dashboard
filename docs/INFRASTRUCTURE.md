# Infrastructure on WITDOM OpenStack tenant

Below you can find a list of VM instances that are already deployed and are related to WITDOM Dashboard app.

* Cloudify manager 3.4 (cloudify-manager) (no authorization, no TLS) - currently supported by dashboard app
* Chef server (chef-server) - Stores Chef data (recipes, roles, attributes etc.) necesarry for configuration of VM instances into desired states during Cloudify deployments
* Application & Web server (web-server) - Runs Nginx and serves the dashboard app through mod_uwsgi. Currently an old version of dashboard is deployed there (to be updated soon).