# WITDOM Dashboard Setup
## 1. Prepare environment
Dashboard app relies on communication with Cloudify Manager. Our manager is (currently) available at 10.10.43.31 and uses Cloudify v3.4. Dashboard application relies on manager's REST client for Python. The app can only connect to managers using Cloudify v3.4.
 
Our Cloudify Manager is secured in two seperate aspects:

* All API calls to Cloudify Manager must use HTTPS.
* It requires authorization of the source producing API calls.

Dashboard app handles HTTPS communication with the help of Cloudify's REST client. However, the system running the dashboard app must provide some configuration in the form of environment variables.
To successfully establish connection with manager running on 172.16.95.138, the following variables must be set:

```bash
$ export CLOUDIFY_SSL_TRUST_ALL=true
$ export CLOUDIFY_USERNAME=witdom_admin
$ export CLOUDIFY_PASSWORD=xlab
```
  
CLOUDIFY_USERNAME and CLOUDIFY_PASSWORD are fixed. They were set at the time of our manager's bootstrap, and are a part of manager's, not dashboard's configuration.
CLOUDIFY_SSL_TRUST_ALL can be set to any value. However, if it's not present, the dashboard app assumes that it needs to verify manager's certificate and will look for another environment variable, CLOUDIFY_SSL_CERT, 
holding the absolute path to a local copy of manager's certificate.

At the moment, dashboard app does not verify manager's certificate. Therefore, it is recommended to set only the above three environment variables.

```bash
$ sudo apt-get install rabbitmq-server
```

## 2. Run the application
1. Create new virtual environment and activate it

    ```bash
    $ virtualenv .dashboardenv
    $ source .dashboardenv/bin/activate
    ```
2. Install requirements while newly created virtual environment is activated

    ```bash
    $ pip install -r requirements.txt
    ```

3. From the root project folder, run following commands 

    ```bash
    $ python manage.py migrate
    $ python manage.py loaddata admin # Creates a default superuser for testing with credentials u=admin, pw=admin
    ```

    To create your own superuser with arbitrary credentials, run
    ```bash
    $ python manage.py createsuperuser
    ```
    and fill in your data. These credentials can later be used to log in into dashboard application. Moreover, this account
    can be used to access administration console at _http://localhost:8000/admin_
    
    To start the app at the default port 8000, run
    ```bash
    $ python manage.py runserver
    ```

4. As deployments of components with Cloudify can be several minutes long, we use [Celery](http://docs.celeryproject.org/en/latest/) to execute them asynchrnously, thus not blocking the end-user of Dashboard application.
To enable this functionality, we have to start a [RabbitMQ server](https://www.rabbitmq.com/) a celery worker daemon prior to executing any deployments with Cloudify:

    ```bash
    $ sudo rabbitmq-server
    $ python manage.py celeryd --loglevel=INFO
    ```
