# WITDOM Dashboard Roadmap

## General
* Chef recipe for local deploy (for demos) - this will be implemented in scope of local deploy of 3-instance interconnected Vagrant environment (dashboard, FreeIPA, SimpleSAMLphp)
* Make dashboard app more WITDOM-friendly - instead of (or in addition to) our logical layer of applications for grouping Cloudify blueprints, introduce more WITDOM-specific terms (PC, KM, ... - whatever category is to be deployed through WITDOM Dashboard)

## Production TODOs
* When deploying the next version of WITDOM Dashboard to OpenStack, use [gunicorn](http://gunicorn.org/) as an application server instead of mod_uwsgi
* Set up [supervisor](http://supervisord.org/)
* Fix serving static files in production
* Fix media files uploaded by user
* Secret key setup and storage